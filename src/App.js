import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Signup from './components/Signup';
// import '../src/styles/style.scss';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Route exact path="/" component={Signup} />
      </div>
    </BrowserRouter>
  );
}

export default App;